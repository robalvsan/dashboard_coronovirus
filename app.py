# ----------------------------------------------------------------------------#
# Imports
# ----------------------------------------------------------------------------#

from flask import Flask, render_template, request
import logging
from logging import Formatter, FileHandler
from forms import *
import json
import plotly
import pandas as pd
import plotly.graph_objects as go
import plotly_express as px

# ----------------------------------------------------------------------------#
# App Config.
# ----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')
# db = SQLAlchemy(app)

# Automatically tear down SQLAlchemy.
'''
@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()
'''

# Login required decorator.
'''
def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login'))
    return wrap
'''


# ----------------------------------------------------------------------------#
# Controllers.
# ----------------------------------------------------------------------------#


@app.route('/')
def home():
    df = pd.read_csv('https://covid19.isciii.es/resources/serie_historica_acumulados.csv', encoding='iso-8859-1')
    df.dropna(thresh=2, inplace=True)
    df = df.rename(columns={'FECHA': 'Fecha', 'CASOS': 'Casos', 'PCR+': 'PCR', 'TestAc+': 'Test'})
    selector = {
        'AN': 'Andalucia', 'AR': 'Aragón', 'AS': 'Asturias', 'IB': 'Islas Baleares', 'CN': 'Islas Canarias',
        'CB': 'Cantabria', 'CM': 'Castilla-La Mancha', 'CL': 'Castilla y Leon', 'CT': 'Catalunya', 'CE': 'Ceuta',
        'VC': 'Comunidad Valenciana', 'EX': 'Extremadura', 'GA': 'Galicia', 'MD': 'Madrid', 'ML': 'Melilla',
        'MC': 'Murcia', 'NC': 'Navarra', 'PV': 'Pais Vasco', 'RI': 'La Rioja'}
    df['CCAA'] = df['CCAA'].apply(lambda x: selector[x])
    df.fillna(0, inplace=True)

    df['Casos'] = df.apply(lambda row: row.Casos + row.PCR + row.Test, axis=1)

    spain_df = df.groupby(['Fecha'], as_index=False).sum()
    spain_df['CCAA'] = 'Spain'
    spain_df['Fecha'] = pd.to_datetime(spain_df['Fecha'], format="%d/%m/%Y")
    spain_df = spain_df.sort_values(by='Fecha')
    max_date = spain_df['Fecha'].max()
    spain_df['Fecha'] = spain_df['Fecha'].dt.strftime('%d/%m/%Y')
    spain_df['Nuevos Casos'] = spain_df['Casos'].diff()
    spain_df['Diferencia Nuevos Casos'] = spain_df['Nuevos Casos'].diff()
    spain_df['Nuevos Casos'].fillna(0, inplace=True)
    spain_df['Diferencia Nuevos Casos'].fillna(0, inplace=True)

    spain_df['Nuevos Fallecidos'] = spain_df['Fallecidos'].diff()
    spain_df['Diferencia Nuevos Fallecidos'] = spain_df['Nuevos Fallecidos'].diff()
    spain_df['Nuevos Fallecidos'].fillna(0, inplace=True)
    spain_df['Diferencia Nuevos Fallecidos'].fillna(0, inplace=True)

    spain_df['Nuevos Hospitalizados'] = spain_df['Hospitalizados'].diff()
    spain_df['Diferencia Nuevos Hospitalizados'] = spain_df['Nuevos Hospitalizados'].diff()
    spain_df['Nuevos Hospitalizados'].fillna(0, inplace=True)
    spain_df['Diferencia Nuevos Hospitalizados'].fillna(0, inplace=True)

    title = 'Datos actualizados hasta día: {}'.format(max_date.date().strftime("%d/%m/%Y"))

    accumulated = ('España - Datos acumulados', json.dumps([
        go.Bar(name='Total Casos', x=spain_df['Fecha'].tolist(), y=spain_df['Casos'].tolist()),
        go.Bar(name='Total Hospitalizados', x=spain_df['Fecha'].tolist(), y=spain_df['Hospitalizados'].tolist()),
        go.Bar(name='Total Fallecidos', x=spain_df['Fecha'].tolist(), y=spain_df['Fallecidos'].tolist())
    ], cls=plotly.utils.PlotlyJSONEncoder))

    new = ('España - Datos nuevos', json.dumps([
        go.Bar(name='Nuevos Casos', x=spain_df['Fecha'].tolist(), y=spain_df['Nuevos Casos'].tolist(),
               text=['Diferencia: {}'.format(dif) for dif in spain_df['Diferencia Nuevos Casos'].tolist()]),
        go.Bar(name='Nuevos Fallecidos', x=spain_df['Fecha'].tolist(), y=spain_df['Nuevos Fallecidos'].tolist(),
               text=['Diferencia: {}'.format(dif) for dif in spain_df['Diferencia Nuevos Fallecidos'].tolist()]),
        go.Bar(name='Nuevos Hospitalizados', x=spain_df['Fecha'].tolist(), y=spain_df['Nuevos Hospitalizados'].tolist(),
               text=['Diferencia: {}'.format(dif) for dif in spain_df['Diferencia Nuevos Hospitalizados'].tolist()]),
    ], cls=plotly.utils.PlotlyJSONEncoder))

    delta = ('España - Deltas de datos', json.dumps([
        go.Bar(name='Diferencia Nuevos Casos', x=spain_df['Fecha'].tolist(),
               y=spain_df['Diferencia Nuevos Casos'].tolist()),
        go.Bar(name='Diferencia Nuevos Fallecidos', x=spain_df['Fecha'].tolist(),
               y=spain_df['Diferencia Nuevos Fallecidos'].tolist()),
        go.Bar(name='Diferencia Nuevos Hospitalizados', x=spain_df['Fecha'].tolist(),
               y=spain_df['Diferencia Nuevos Hospitalizados'].tolist()),
    ], cls=plotly.utils.PlotlyJSONEncoder))

    gapminder = (
        'Evolución Comunidades Autónomas',
        json.dumps(
            px.scatter(df,
                       x="Casos",
                       y="Fallecidos",
                       animation_frame="Fecha",
                       animation_group="CCAA",
                       size="Hospitalizados",
                       color="CCAA",
                       hover_name="CCAA",
                       range_x=[0, df['Casos'].max() + 1000],
                       range_y=[0, df['Fallecidos'].max() + 100]
                       ), cls=plotly.utils.PlotlyJSONEncoder)
    )

    return render_template('pages/placeholder.home.html',
                           title=title,
                           gapminder=gapminder,
                           accumulated=accumulated,
                           new=new,
                           delta=delta)


@app.route('/about')
def about():
    return render_template('pages/placeholder.about.html')


@app.route('/login')
def login():
    form = LoginForm(request.form)
    return render_template('forms/login.html', form=form)


@app.route('/register')
def register():
    form = RegisterForm(request.form)
    return render_template('forms/register.html', form=form)


@app.route('/forgot')
def forgot():
    form = ForgotForm(request.form)
    return render_template('forms/forgot.html', form=form)


# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    # db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

# ----------------------------------------------------------------------------#
# Launch.
# ----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
    app.run()

# Or specify port manually:
'''
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
'''
